# Actemplater

Aruba Central (AC) text template preprocessor 

This programm is only needed, cause aruba text template is really constrained. If it would have had a loop iterator and logical operators, this program would have never been written. 

## Defintion in Template

- iface = interface
- nf = network function
- nac = network access control / iface will be configured to use nac
- lag = link aggregation group / iface will be part of an lag and will be a trunk 
- trunk = switch port will be configured as trunk allowed vlan all
- ap = accesspoint, with trunk alloed vlan all (may be more specific in actual deployment)
- access = switchport will be an access port

## Variables

- _id_port[0-52] will be used as vlan id or lag id

## Remarks

Do not use `user admin group administrators password plaintext %_var_password%`
in production. 
Use `user admin group administrators password ciphertext XXXXXXXXXXXXXXXXX` instead.


## Roadmap

### Testdata

[Tip Nr 10 - Timestamp 20:00](https://youtu.be/1-o-iJlL4ak?si=-6ETZrWNlGHs96A4)
Use testdata with pairs of files



### Implemeted Features

- Add feature virtual swicthing framework (VSF) 
  - Static VSF Member config
  - Add interface variables to allow configuration of the ports
  

## Resources

### Aruba

Sadly I am unable to find a full documetation about the aruba text template language. So This are the resources I found out about it.  

- [Configuring AOS-CX Using Templates](https://help.central.arubanetworks.com/2.5.4/documentation/online_help/content/nms/aos-cx/get-started/prov-tmplt-cx.htm?Highlight=text%20template)
- [Using Configuration Templates for Aruba Switch Management](https://help.central.arubanetworks.com/2.5.2/documentation/online_help/content/switches/cfg/cfg-template/prov_template.htm)
- [AOS-CX VSF Stack](https://help.central.arubanetworks.com/2.5.3/documentation/online_help/content/switches/switch-stack/sw-stk-cx.htm)
- [Sample Template and Variables Files](https://help.central.arubanetworks.com/2.5.4/documentation/online_help/content/sd-branch/cfg-templates/cfg-tmpl-smpl-tmpl.htm)
- [AC Userguide 2.4.9](https://help.central.arubanetworks.com/2.4.9/documentation/online_help/content/pdfs/aruba_central_user_guide.pdf)


### Tips for develoment
