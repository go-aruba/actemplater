-- basic_pre-template_V99.11.20240710-1.txt --
{{- $Maintainer := "e-mail@domain" }}
Version {{ .Version }}.{{ .Build }}
hostname: %_sys_hostname%
pvid: %_var_pvid%
serial: %_sys_serial%
mac: %_sys_lan_mac%
site: %_var_site%
location: %_var_location%
spanning-tree prio: %_var_stp_prio%
model: %_var_model%
Maintainer: {{ $Maintainer }}
{{- define "interface" }}
{{- $i := . }}
{{- $n := printf "%02d" $i }}  
interface 1/1/{{ $i }}
{{- end }}
{{- define "lag" }}
{{- $i := . }}
interface lag {{ $i }}
{{- end }}

{{- range $i := Iterate 2 }}
  {{- template "lag" $i}}
{{- end }}

{{- range $i := Portrange 1 2 }}
  {{- template "interface" $i}}
{{- end }}
-- expected --

Version 99.11.20240710-1
hostname: %_sys_hostname%
pvid: %_var_pvid%
serial: %_sys_serial%
mac: %_sys_lan_mac%
site: %_var_site%
location: %_var_location%
spanning-tree prio: %_var_stp_prio%
model: %_var_model%
Maintainer: e-mail@domain
interface lag 1
interface lag 2  
interface 1/1/{1 1}  
interface 1/1/{2 1}