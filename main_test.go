package main

import (
	"bytes"
	"path/filepath"
	"testing"
	"text/template"

	"golang.org/x/tools/txtar"
)

func TestParseFileName(t *testing.T) {
	tests := []struct {
		input           string
		expectedVersion string
		expectedBuild   string
		expectError     bool
	}{
		{"mystring_V0.11.20240710-0.txt", "0.11", "20240710-0", false},
		{"more_mystring_V0.11.20240710-1.txt", "0.11", "20240710-1", false},
		{"another_example_V0.11.20240710-2.txt", "0.11", "20240710-2", false},
		{"yet_another_example_V0.11.20240710-10.txt", "0.11", "20240710-10", false},
		{"my-switch_ac-template_V0.11.20240710-0.txt", "0.11", "20240710-0", false},
		{"my-switch_ac-template_V0.11.20240710-1.txt", "0.11", "20240710-1", false},
		{"my-sw_ac-template_V0.11.20240710-2.txt", "0.11", "20240710-2", false},
		{"my-sw_ac-template_V0.11.20240710-22.txt", "0.11", "20240710-22", false},
		{"invalid_format.txt", "", "", true},
		{"missing_v_0.11.20240710-0.txt", "", "", true},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			info, err := parseFileName(test.input)
			if test.expectError {
				if err == nil {
					t.Errorf("expected an error but got nil")
				}
			} else {
				if err != nil {
					t.Errorf("did not expect an error but got %v", err)
				}
				if info.Version != test.expectedVersion {
					t.Errorf("expected version %s, got %s", test.expectedVersion, info.Version)
				}
				if info.Build != test.expectedBuild {
					t.Errorf("expected build %s, got %s", test.expectedBuild, info.Build)
				}
			}
		})
	}
}

func TestTemplateRendering(t *testing.T) {
	//Defining Template Functions
	funcMap := template.FuncMap{
		"Iterate":         iterate,
		"IterateFromTo":   iterateFromTo,
		"Portrange":       portrangeMember1,
		"PortrangeMember": portrangeMember2,
	}

	txtarFiles, _ := filepath.Glob("testdata/*.txt")
	if len(txtarFiles) == 0 {
		t.Fatalf("no testdata")
	}

	for _, txtarFile := range txtarFiles {
		t.Run(filepath.Base(txtarFile), func(t *testing.T) {
			a, err := txtar.ParseFile(txtarFile)
			if err != nil {
				t.Fatal(err)
			}
			if len(a.Files) != 2 || a.Files[1].Name != "expected" {
				t.Fatalf(`%s: want two files, second name "expected"`, txtarFile)
			}

			tmplFile := a.Files[0]
			tmpl := template.Must(template.New(filepath.Base(tmplFile.Name)).Funcs(funcMap).Parse(string(tmplFile.Data)))

			info, err := parseFileName(tmplFile.Name)
			if err != nil {
				t.Fatal(err)
			}
			var output bytes.Buffer

			err = tmpl.Execute(&output, info)
			if err != nil {
				t.Fatalf("Template execution failed: %v", err)
			}

			actual := output.String()
			expected := string(a.Files[1].Data)

			if actual != expected {
				t.Errorf("Expected %q\n\n--> but got %q\n", expected, actual)
			}
		})
	}
}
