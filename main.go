/*
This programm will preprocess template to generate aruba central text templates.
It is created quick and dirty but it is used in real production environment,

Since I decided that development will happen in public donain.
Some Parts of the Programm are designed to conceal real used values.
*/
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"strings"
	"text/template"
)

const VERSION = "0.0.2"

type Info struct {
	Version string
	Build   string
}

type Switch struct {
	Model string
	Ports []int
}

type Data struct {
	Portid      int
	Stackmember int
}

func main() {
	in := flag.String("i", "", "Input file for preprocessing")
	v := flag.Bool("v", false, "Prints out Version")
	flag.Parse()

	if *v {
		fmt.Println("Version: ", VERSION)
		os.Exit(0)
	}

	if *in == "" {
		fmt.Println("Input file name MUST be provided")
		flag.Usage()

	}

	//Defining Template Functions
	funcMap := template.FuncMap{
		"Iterate":         iterate,
		"IterateFromTo":   iterateFromTo,
		"Portrange":       portrangeMember1,
		"PortrangeMember": portrangeMember2,
	}

	out := strings.Replace(path.Base(*in), "pre", "ac", 1)

	// Openfile output file
	w, err := os.Create(out)
	if err != nil {
		log.Fatal(fmt.Errorf("failed to open file %s: %w", out, err))
	}
	defer w.Close()

	// Create Template and assign necessary function to it
	info, err := parseFileName(*in)
	if err != nil {
		log.Fatal(err)
	}
	name := path.Base(*in)
	tmpl := template.Must(template.New(name).Funcs(funcMap).ParseFiles(*in))

	// Execute template with info struct inculded
	if err := tmpl.Execute(w, info); err != nil {
		log.Fatal(fmt.Errorf("failed to execute: %w", err))
	}

}

// Get Version and Build Number out of the Filename
func parseFileName(input string) (Info, error) {
	var result Info

	// Check if the string ends with ".txt"
	if !strings.HasSuffix(input, ".txt") {
		return result, fmt.Errorf("invalid format: %s", input)
	}

	// Remove the ".txt" suffix
	base := strings.TrimSuffix(input, ".txt")

	// Find the first occurrence of "_V" in the remaining string
	vIndex := strings.Index(base, "_V")
	if vIndex == -1 {
		return result, fmt.Errorf("invalid format: %s", input)
	}

	// Extract the part starting from "_V"
	versionAndBuild := base[vIndex+2:]

	// Split the extracted part by the last dot to separate version and build
	lastDotIndex := strings.LastIndex(versionAndBuild, ".")
	if lastDotIndex == -1 {
		return result, fmt.Errorf("invalid format: %s", input)
	}

	// Set the extracted version and build to the result struct
	result.Version = versionAndBuild[:lastDotIndex]
	result.Build = versionAndBuild[lastDotIndex+1:]

	return result, nil
}

// Template Functions

func portrange(m, s, e int) []Data {
	d := []Data{}
	for i := s; i <= e; i++ {
		d = append(d, Data{
			Portid:      i,
			Stackmember: m,
		})

	}
	return d
}

func portrangeMember1(s, e int) []Data {
	return portrange(1, s, e)
}

func portrangeMember2(s, e int) []Data {
	return portrange(2, s, e)
}

func iterateFromTo(s, e int) (Items []int) {
	for i := s; i <= e; i++ {
		Items = append(Items, i)
	}
	return Items
}

func iterate(count int) []int {
	return iterateFromTo(1, count)
}
